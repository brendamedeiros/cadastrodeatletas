package view;
import model.Atleta;
import model.Endereco;
import controller.ControladorAtleta;
import java.util.Scanner;

public class TelaAtleta {

	public static void main(String[] args) {
		int opcao = -1;
		Scanner leitor = new Scanner(System.in);
		Atleta umAtleta = null;
		Endereco umEndereco = null;
		ControladorAtleta umControlador = null;
		
		while(opcao != 0){
			System.out.println("Cadastro Atleta \n"
					+ "1 - Adicionar Atleta\n"
					+ "2 - Listar Atleta\n"
					+ "3 - Remover Atleta\n"
					+ "0 - Fechar Programa");
			
			opcao = leitor.nextInt();
			
			switch(opcao){
			
			case 1:
				System.out.println("Informe o nome do Atleta:");
				String nome = leitor.nextLine();
				
				umAtleta = new Atleta(nome);
				
				System.out.println("Informe a idade do Atleta:");
				String idade = leitor.next();
				
				umAtleta.setIdade(idade);
				
				System.out.println("Informe o peso do Atleta:");
				String peso = leitor.next();
				
				umAtleta.setPeso(peso);
				
				System.out.println("Informe o estado do Atleta:");
				String estado = leitor.next();
				
				umEndereco = new Endereco(estado);
				
				System.out.println("Informe a cidade do Atleta:");
				String cidade = leitor.next();
				
				umEndereco.setCidade(cidade);
				
				System.out.println("Informe o bairro do Atleta:");
				String bairro = leitor.next();
				
				umEndereco.setBairro(bairro);
				
				System.out.println("Informe o numero do Atleta:");
				String numero = leitor.next();
				
				umEndereco.setNumero(numero);
				
				System.out.println("Informe o logradouro do Atleta:");
				String logradouro = leitor.next();
				
				umEndereco.setLogradouro(logradouro);
				
				//setar endereco em Atleta
				umAtleta.setEndereco(umEndereco);
				
				//adicionar atleta
				umControlador.adicionar(umAtleta);
				
				break;
				
			case 2:
				System.out.println("Informe o nome do Atleta que deseja buscar:");
				String umNome = leitor.next();
				umAtleta = umControlador.buscar(umNome); //busca pelo nome e retorna o atleta
				
				System.out.println("Dados do Atleta:"
						+ "\nNome:" + umAtleta.getNome()
						+ "\nIdade:" + umAtleta.getIdade()
						+ "\nPeso:" + umAtleta.getPeso()
						+ "\nEstado:" + umEndereco.getEstado()
						+ "\nCidade:" + umEndereco.getCidade()
						+ "\nBairro:" + umEndereco.getBairro()
						+ "\nLogradouro:" + umEndereco.getLogradouro()
						+ "\nNumero:" + umEndereco.getNumero());
				
				break;
				
			case 3:
				System.out.println("Informe o nome do Atleta que deseja remover:");
				String umNome1 = leitor.next();
				umAtleta = umControlador.buscar(umNome1); //busca pelo nome e retorna o atleta
				
				umControlador.remover(umAtleta);
				
				break;
				
			default:
				System.out.println("Valor incorreto, digite novamente. [1][2][3][0]");
				opcao = leitor.nextInt();		
				
			}//switch-case
		} //while

	}

}
