package model;

public class Futebolista extends Atleta {
	
	private int chuteForte; // 1-Chute Forte  / 0 - chute fraco
	private int tamanhoPe;
	
	public Futebolista(String nome, int tamanhoPe) {
		super(nome);
		this.tamanhoPe = tamanhoPe;
	}
	
	public int getChuteForte() {
		return chuteForte;
	}
	public void setChuteForte(int chuteForte) {
		this.chuteForte = chuteForte;
	}
	public int getTamanhoPe() {
		return tamanhoPe;
	}
	public void setTamanhoPe(int tamanhoPe) {
		this.tamanhoPe = tamanhoPe;
	}
	

}
