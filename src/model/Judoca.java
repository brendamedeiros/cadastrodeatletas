package model;

public class Judoca extends Atleta{
	
	private String faixa;
	private String categoria;
	private boolean bomSolo;
		
	public Judoca(String nome, String faixa) {
		super(nome);
		this.faixa = faixa;
	}
	public String getFaixa() {
		return faixa;
	}
	public void setFaixa(String faixa) {
		this.faixa = faixa;
	}
	public String getCategoria() {
		return categoria;
	}
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	public boolean isBomSolo() {
		return bomSolo;
	}
	public void setBomSolo(boolean bomSolo) {
		this.bomSolo = bomSolo;
	}
	
}
